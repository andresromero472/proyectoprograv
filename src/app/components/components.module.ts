import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { NavbuttonComponent } from './navbutton/navbutton.component';


@NgModule({
  declarations: [
    NavbuttonComponent,
    HeaderComponent, 
  ],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [
    NavbuttonComponent,
    HeaderComponent,
  ]

})
export class ComponentsModule { }
