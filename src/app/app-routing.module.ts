import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LocationPickerComponent } from './shared/pickers/location-picker/location-picker.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'pick',
    loadChildren: () =>import('./shared/pickers/location-picker/location-picker.component').then ( m => LocationPickerComponent)
  },
  {
    path: 'restaurantes',
    children: [
      {
        path: '',
        loadChildren: () => import('./restaurantes/restaurantes.module').then( m => m.RestaurantesPageModule)
      },
      {
        path:':restauranteID',
        loadChildren: () => import('./restaurantes/editar-restaurante/editar-restaurante.module').then( m => m.EditarRestaurantePageModule)
      }
    ]
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then( m => m.AccountPageModule)       
  },
  {
    path: 'map',
    loadChildren: () => import('./map/map.module').then( m => m.MapPageModule)
  },
  {
    path: 'configuracion',
    loadChildren: () => import('./configuracion/configuracion.module').then( m => m.ConfiguracionPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
