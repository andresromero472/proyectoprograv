export interface coordenadas{
    lat: number;
    lng: number;
}
export interface placeLocation extends coordenadas{
    address: string;
    staticMapImageUrl: string;
}