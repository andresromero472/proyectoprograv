import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';


export const firebaseConfig = {
    apiKey: 'AIzaSyAY79UI6aiwqx-rgPKPoB49NjefY-I_FdQ',
    authDomain: 'programacionv-2e6fb.firebaseapp.com',
    databaseURL: 'https://programacionv-2e6fb.firebaseio.com',
    projectId: 'programacionv-2e6fb',
    storageBucket: 'programacionv-2e6fb.appspot.com',
    messagingSenderId: '690479297224',
    appId: '1:690479297224:web:6ffd3eef88714555446bfd',
    measurementId: 'G-JMZQ2E3263'
  };

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    ComponentsModule, 
    HttpClientModule, 
    AngularFireModule, 
    AngularFirestoreModule, 
    AngularFireModule.initializeApp(firebaseConfig)  
  ],

  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
