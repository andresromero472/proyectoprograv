import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  logIn: FormGroup;
  constructor() { }

  ngOnInit() {
    this.logIn =new FormGroup({
      user: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      password: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      })
    });
  }
  LogInUser(){
    
  }
}