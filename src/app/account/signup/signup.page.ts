import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServiceService } from 'src/app/restaurantes/service.service';
import { ServicioCuentaService } from '../servicio-cuenta.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  signupForm: FormGroup;
  constructor(private servicio: ServicioCuentaService,) { }

  ngOnInit() {
    this.signupForm =new FormGroup({
      email: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      username: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      password: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      })
    });
  }
  onClickSignUp(){
    this.servicio.SignUp(
      this.signupForm.value.email,
      this.signupForm.value.username,
      this.signupForm.value.password
    )
  }
}
