import { TestBed } from '@angular/core/testing';

import { ServicioCuentaService } from './servicio-cuenta.service';

describe('ServicioCuentaService', () => {
  let service: ServicioCuentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicioCuentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
