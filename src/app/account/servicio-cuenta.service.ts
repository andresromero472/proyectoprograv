import { Injectable } from '@angular/core';
import { AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ServicioCuentaService {

  constructor( private afs: AngularFirestore) { }
  SignUp(rcorreo: string, rusuario: string, rpassword: string){
    console.log(rcorreo);
    console.log(rusuario);
    console.log(rpassword);
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/usuarios').add({
        correo: rcorreo,
        user: rusuario,
        password: rpassword
      })
      .then((res) => { resolve(res); }, err => reject(err));
    });
  }

}
