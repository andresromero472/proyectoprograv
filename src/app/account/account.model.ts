export interface restaurante{
    correo: string
    username: string;
    password: string;
}
export class restaurante implements restaurante{
    constructor(
        public correo: string,
        public username: string,
        public password: string
    ){}
}