import { Component, OnInit } from '@angular/core';
import { restaurante } from './restaurantes.model';
import { ServiceService } from './service.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.page.html',
  styleUrls: ['./restaurantes.page.scss'],
})
export class RestaurantesPage implements OnInit {
  private restaurantes: Observable<restaurante[]>;
  textoBuscar = "";
  constructor(private servicio: ServiceService) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.restaurantes = this.servicio.getAllRestaurante();
  }
  buscar(event){
    //console.log(event);
    this.textoBuscar = event.detail.value;
  }
  
}
