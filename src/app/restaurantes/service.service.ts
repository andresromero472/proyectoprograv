import { Injectable } from '@angular/core';
import { restaurante } from './restaurantes.model';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection,AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private restaurante: Observable<restaurante[]>;
  private restauranterf: AngularFirestoreCollection<restaurante>;
  constructor(
    private afs: AngularFirestore
  ) {
  }
  getRestaurante(detalleId: string): AngularFirestoreDocument<restaurante> {
    return this.afs.collection('listaRestaurantes').doc(detalleId);
  }
  getAllRestaurante(){
    this.restauranterf = this.afs.collection('listaRestaurantes');
    this.restaurante = this.restauranterf.valueChanges();
    return this.restaurante;
  }
  addRestaurante(rnombre: string, rcategoria: string, rdescripcion: string, rdireccion: string, rfoto: string ){
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('/listaRestaurantes').add({
        nombre: rnombre,
        categoria: rcategoria,
        descripcion: rdescripcion,
        direccion: rdireccion,
        foto: rfoto,
      })
      .then((res) => { resolve(res); }, err => reject(err));
    });
  }
}
