import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RestaurantesPage } from './restaurantes.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantesPage
  },
  {
    path: 'nuevo-restaurante',
    loadChildren: () => import('./nuevo-restaurante/nuevo-restaurante.module').then( m => m.NuevoRestaurantePageModule)
  },
  {
    path: 'editar-restaurante',
    loadChildren: () => import('./editar-restaurante/editar-restaurante.module').then( m => m.EditarRestaurantePageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestaurantesPageRoutingModule {}
