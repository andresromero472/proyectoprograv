import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarRestaurantePageRoutingModule } from './editar-restaurante-routing.module';

import { EditarRestaurantePage } from './editar-restaurante.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditarRestaurantePageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule

  ],
  declarations: [EditarRestaurantePage]
})
export class EditarRestaurantePageModule {}
