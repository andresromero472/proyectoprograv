import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { ServiceService } from '../service.service';
import { restaurante } from '../restaurantes.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-editar-restaurante',
  templateUrl: './editar-restaurante.page.html',
  styleUrls: ['./editar-restaurante.page.scss'],
})
export class EditarRestaurantePage implements OnInit {
  restauranteActual: restaurante;
  edit_restaurant: FormGroup;
  constructor(    
    private route: ActivatedRoute,
    private servicio: ServiceService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap =>{
      if(!paramMap.has('restauranteID')){
        return;
      }
      const restauranteId = parseInt(paramMap.get('restauranteID'));
      this.restauranteActual = this.servicio.GetProduct(restauranteId);
      console.log(this.restauranteActual);
    });
    this.edit_restaurant =new FormGroup({
      nombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      descripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      direccion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      categoria: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      image: new FormControl(null, {
        updateOn: "blur",
      })
    });
    
  }
  onClick(){
    console.log(this.edit_restaurant)
    this.servicio.addRestaurante(
      this.edit_restaurant.value.nombre,
      this.edit_restaurant.value.categoria,
      this.edit_restaurant.value.descripcio,
      this.edit_restaurant.value.direccion,
      this.edit_restaurant.value.image,

    )
  }
}


