export interface restaurante{
    id: string;
    nombre: string;
    categoria: string;
    descripcion: string;
    direccion: string;    
    foto: string;
}
export class restaurante implements restaurante{
    constructor(
        public id: string,
        public nombre: string,
        public categoria: string,
        public descripcion:string,
        public direccion: string,
        public foto: string 
    ){}
}