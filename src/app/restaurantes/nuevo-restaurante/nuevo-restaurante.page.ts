import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-nuevo-restaurante',
  templateUrl: './nuevo-restaurante.page.html',
  styleUrls: ['./nuevo-restaurante.page.scss'],
})
export class NuevoRestaurantePage implements OnInit {
new_restaurant: FormGroup;
  constructor(private servico: ServiceService) { }

  ngOnInit() {
    this.new_restaurant =new FormGroup({
      nombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      descripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      direccion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      categoria: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      foto: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      })
    });
  }
  onClickAdd(){
    this.servico.addRestaurante(
      this.new_restaurant.value.nombre,
      this.new_restaurant.value.categoria,
      this.new_restaurant.value.descripcion,
      this.new_restaurant.value.direccion,
      this.new_restaurant.value.foto
    )
  }
}
