import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevoRestaurantePageRoutingModule } from './nuevo-restaurante-routing.module';

import { NuevoRestaurantePage } from './nuevo-restaurante.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevoRestaurantePageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  declarations: [NuevoRestaurantePage]
})
export class NuevoRestaurantePageModule {}
